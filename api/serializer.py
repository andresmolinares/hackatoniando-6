from rest_framework.response import Response

from api.models import Product
from rest_framework import serializers, status


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'image', 'url')
