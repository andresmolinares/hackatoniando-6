from django.shortcuts import render
from rest_framework import viewsets, permissions, status

# Create your views here.
from rest_framework.response import Response

from api.models import Product
from api.serializer import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    # pagination_class = None
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
